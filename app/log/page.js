"use client";
import PrimaryText from "../../components/PrimaryText";
import Link from "next/link";
import Box from '@mui/material/Box';
import { DataGridPremium } from '@mui/x-data-grid-premium';
import { ThemeProvider } from '@mui/material/styles';
import {whiteTextBlackPaper} from "../../components/themes";
export default function Page(){

    return(
            <>
            <PrimaryText childrenMenu={
                <>
                <Link href="/" alt={"10144"}>{"<<"}</Link> <br/>
                </>
            }
            childrenBody={
                <ThemeProvider theme={whiteTextBlackPaper}>
                <Box sx={{height:"85%", width:"100%"}}>
                    <DataGridPremium
                        columns={[]}
                        rows={[]}
                        hideFooter={true} />
                </Box>
                </ThemeProvider>
            }/>
            </>
            )
}